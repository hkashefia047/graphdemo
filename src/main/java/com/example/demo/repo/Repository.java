package com.example.demo.repo;

import com.example.demo.model.MyGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Repository extends JpaRepository<MyGraph,Long> {
}
