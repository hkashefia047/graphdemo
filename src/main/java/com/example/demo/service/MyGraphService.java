package com.example.demo.service;

import com.example.demo.mapper.GraphMapper;
import com.example.demo.model.MyGraph;
import com.example.demo.model.requestdto.MyGraphRequestDto;
import com.example.demo.model.responseDto.EdgeResponseDto;
import com.example.demo.model.responseDto.MyGraphResponseDto;
import com.example.demo.repo.Repository;
import javassist.NotFoundException;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import javax.xml.bind.ValidationException;
import java.util.List;

@Service
public class MyGraphService {

    private GraphMapper mapper;
    private Repository repository;

    public MyGraphService(GraphMapper mapper, Repository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }
    public MyGraph saveGraph(MyGraphRequestDto dto) throws ValidationException {
        if (dto!=null){
             MyGraph graph=mapper.requestToEntity(dto);
             return repository.save(graph);
        }else {
            throw new ValidationException("validation failed!");
        }
    }

    public MyGraphResponseDto getById(Long id)throws NotFoundException {
            MyGraph graph=repository.findById(id).get();
            if (graph!=null){
                return mapper.entityToResponse(graph);
            }else {
                throw new NotFoundException("the graph is not in db");
            }
    }

    public void shortestPathInGraph(Integer x,Integer y) throws NotFoundException {
        MyGraphResponseDto responseDto=getById(1L);
        List<Integer> nodes=responseDto.getNode();
        List<EdgeResponseDto>edges=responseDto.getEdges();
         DefaultDirectedGraph<Integer, DefaultEdge> g = MyDirectedGraph(nodes, edges);

        DijkstraShortestPath dijkstraShortestPath=new DijkstraShortestPath(g);

        List<Integer>shortestPath=dijkstraShortestPath.getPath(x,y).getVertexList();

        for (int i = 0; i <shortestPath.size() ; i++) {
            System.out.println(shortestPath.get(i));
        }
    }

    public DefaultDirectedGraph<Integer, DefaultEdge> MyDirectedGraph(List<Integer> nodes, List<EdgeResponseDto> edges) {
        DefaultDirectedGraph<Integer, DefaultEdge> g = new DefaultDirectedGraph<Integer, DefaultEdge>(DefaultEdge.class);

        nodes.forEach(x->g.addVertex(x));

        edges.forEach(x->g.addEdge(x.getSource(),x.getDestination()));
        return g;
    }
}
