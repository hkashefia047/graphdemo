package com.example.demo.mapper;
import com.example.demo.model.MyGraph;
import com.example.demo.model.requestdto.MyGraphRequestDto;

import com.example.demo.model.responseDto.MyGraphResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
public interface GraphMapper {
    GraphMapper mapper= Mappers.getMapper(GraphMapper.class);

    MyGraph requestToEntity(MyGraphRequestDto dto);
    MyGraphResponseDto entityToResponse(MyGraph graph);

}
