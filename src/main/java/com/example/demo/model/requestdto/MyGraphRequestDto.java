package com.example.demo.model.requestdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyGraphRequestDto {
    private List<Integer> node;
    private List<EdgeRequestDto>edges;
}
