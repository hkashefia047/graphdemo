package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MyGraph {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ElementCollection
    private List<Integer>node;
    @ElementCollection(targetClass = Edge.class,fetch = FetchType.EAGER)
    @CollectionTable
    private List<Edge>edges;
}
