package com.example.demo.model.responseDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyGraphResponseDto {
    private List<Integer> node;
    private List<EdgeResponseDto>edges;
}
