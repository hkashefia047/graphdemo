package com.example.demo.controller;

import com.example.demo.model.MyGraph;
import com.example.demo.model.requestdto.MyGraphRequestDto;
import com.example.demo.service.MyGraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyGraphController {
    @Autowired
    MyGraphService service;


    @PostMapping(value = "/topology",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Long>create(@RequestBody MyGraphRequestDto dto){
        try{
          MyGraph graph  =service.saveGraph(dto);
          return new ResponseEntity<>(graph.getId(),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

//    @RequestBody MyVariables var
    @PostMapping(value = "/calculatepath")
    public ResponseEntity<Long>calculate(@RequestParam Integer PathSource,@RequestParam Integer PathDestination){
        try{
//            Integer x=var.getPathSource();
//            Integer y=var.getPathDestination();
            service.shortestPathInGraph(PathSource,PathDestination);
            return new ResponseEntity<>(1L,HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
